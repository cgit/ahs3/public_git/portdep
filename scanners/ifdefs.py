#!/usr/bin/env python
#
#   Copyright (c) 2012, Al Stone <ahs3@ahs3.net>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of version 2 (only) of the GNU General
#   Public License as published by the Free Software Foundation.
#

import os
import os.path
import re
import subprocess
import sys

def main():
    '''this scanner counts up the number of instances of #ifdef that
       have an architecture name in them.
    '''
    path = sys.argv[2]

    arches = ['__alpha__', '__alpha_ev[0-9]+__',
              '__amd64__', '__x64_64__',
              '__arm__', '__thumb__', '__TARGET_ARM_[0-9A-Z]+__',
              '__convex__', '__convex_[0-9]+__',
              '__epiphany__',
              '__hppa__',  '__PA_RISC[0-9]_[0-9]__', '__HPPA[0-9]+__', 
                           '__PA[0-9][0-9]00__',
              '__i[3-6]86__',
              '__ia64__',
              '__m68k__', '__mc680[0-9]0__', '__MC680[0-9]0__',
              '__mips__', '__MIPS_ISA_MIPS[0-9]__',
              '__powerpc__', '__ppc[0-9]*__',
              '__sparc__',
              '__sh[0-9]__',
              '__s390__', '__s390x__',
              '__TMS320[0-9A-Z]*__',
              '__TMS470__',
             ]

    cmd = 'grep -hr \'^#if[ \\t]*def.*\' %s' % path
    count = 0
    findings = ""
    try:
        prog = re.compile('|'.join(arches))
        findings = subprocess.check_output(cmd, shell=True)
	if findings:
            for ii in findings.split('\n'):
                if prog.search(ii):
                    count += 1
    except subprocess.CalledProcessError, e:
        if e.returncode == 1:           # usually means nothing found
                count = 0
        else:
                count = -1
        
    result = 'Ifdef-Arch: %d' % count
    return result

if __name__ == '__main__':
    result = main()

print result
