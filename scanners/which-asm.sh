#!/bin/bash
#
#   Copyright (c) 2012, Al Stone <ahs3@ahs3.net>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of version 2 (only) of the GNU General
#   Public License as published by the Free Software Foundation.
#

scanner="Which-Asm"

workdir="$1"
src="$2"

WHICHASM=whichasm

declare -i count
count=0
if [ -d "$src" ]
then
        find "$src" \( -name '*\.[cCsShH]' \
             -o -name '*\.cc' \
             -o -name '*\.cxx' \
             -o -name '*\.cpp' \
             \) -print > /tmp/$$
        if [ -s /tmp/$$ ]
        then
                count=$(cat /tmp/$$ | (
                        count=0
                        read line
                        while [ ! -z $line ]
                        do
                                res=$(${WHICHASM} $line)
                                if [ "$(echo $res | grep ^error:)" ] || \
                                   [ "$(echo $res | grep '.*: unknown')" ]
                                then
                                        true
                                else
                                        count+=1
                                fi
                                read line
                        done
                        echo $count
                ))
        fi
        rm -f /tmp/$$
else
        count="-1"
fi
echo "${scanner}: $count"

