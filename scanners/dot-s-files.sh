#!/bin/bash
#
#   Copyright (c) 2012, Al Stone <ahs3@ahs3.net>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of version 2 (only) of the GNU General
#   Public License as published by the Free Software Foundation.
#

scanner="Dot-S-Files"

workdir="$1"
src="$2"

if [ -d "$src" ]
then
        count=$(find "$src" -name '*\.[sS]' -print | wc -l)
else
        count="-1"
fi
echo "${scanner}: $count"

