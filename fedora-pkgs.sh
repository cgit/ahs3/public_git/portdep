#!/bin/sh
#
#   Copyright (c) 2012, Al Stone <ahs3@ahs3.net>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of version 2 (only) of the GNU General
#   Public License as published by the Free Software Foundation.
#
#
#       generate a list of all known fedora packages
#

(
        yum list -q all |
                grep -v "^Installed Packages" |
                grep -v "^Available Packages" |
                cut -d' ' -f1 |
                cut -d. -f1 |
                sort -b |
                uniq
) > fedora.pkgs
