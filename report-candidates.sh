#!/bin/bash
#
#   Copyright (c) 2012, Al Stone <ahs3@ahs3.net>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of version 2 (only) of the GNU General
#   Public License as published by the Free Software Foundation.
#
#
#       Sort through the raw results of a scan to find candidates
#       for examination -- i.e., those packages that may require
#       new or changed assembler on a new architecture, whether to
#       just plain work or for optimization.
#

if [ "$#" -lt 1 ]
then
        echo "usage: ./report-candidates.sh <results-file>"
        exit 1
fi

data="$1"
if [ ! -s "$data" ]
then
        echo "? has to be something to work with"
        exit 1
fi

cat "$data" | (
        declare -i candidates count issues
        candidates=0
        count=0
        issues=0
        while IFS='' read -r line || [ -n "$line" ]
        do
                case "$line" in
                Package:*)
                        if [ $issues -gt 0 ]
                        then
                                candidates+=1
                                [ $issues -gt 0 ] && echo $pkg $version
                        fi
                        count+=1
                        issues=0
                        pkg=$(echo $line | sed 's/Package: //')
                        ;;
                Source:*)
                        version=$(echo $line | sed 's/Source: //' | \
                                  sed "s/${pkg}-//")
                        ;;
                Dot-S-Files:*)
                        value=$(echo $line | sed 's/Dot-S-Files: //')
                        issues+=$(expr $value)
                        ;;
                Ifdef-Arch:*)
                        value=$(echo $line | sed 's/Ifdef-Arch: //')
                        issues+=$(expr $value)
                        ;;
                Which-Asm:*)
                        value=$(echo $line | sed 's/Which-Asm: //')
                        issues+=$(expr $value)
                        ;;
                esac
        done
        echo "$count Source Packages"
        echo "$candidates Candidates for a further look"
)


