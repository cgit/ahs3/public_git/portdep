/*
 * Classifier for ARM assembly language
 *
 * Copyright (C) 2012 Red Hat, Inc.
 *
 */

/* ARM opcodes usually have a format like this:
 *    mnemonic <Rd>, <Ra>, <Rb>...
 * Register names include the following:
 *
 *    32-bit GPRS: R0-R15 (fp, ip, sp, lr, pc)
 *    32-bit shadow FIQ mode GPRS: R8_FIQ-R14_FIQ
 *    32-bit shadow SVC mode GPRS: R13_SVC, R14_SVC
 *    32-bit shadow ABT mode GPRS: R13_ABT, R14_ABT
 *    32-bit shadow IRQ mode GPRS: R13_IRQ, R14_IRQ
 *    32-bit program status registers: APSR, SPSR, CPSR (including shadow regs)
 *    (also APCS defined a1-a4, v1-v8, s0-s32, d0-d16, etc.)
 *    (not doing floating point registers at this stage)
 */

#include "arm.h"
#include <stdio.h>
#include <sys/types.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>

#include "classifier.h"

int classifier_arm(char *token_name)
{
	const struct arm_opcode *opcode;
	const struct arm_reg *reg;

	for (opcode=arm_opcodes;opcode->assembler;opcode++) {
		// TODO - catch variants of assembly language mnemonics
		// previously limited to opcode->assembler length
		// need to catch assembly that has appended letters
		if (strlen(token_name) == strlen(opcode->assembler))
			if (0 == strncasecmp(token_name, opcode->assembler,
					     strlen(opcode->assembler))) {
				//printf("opcode: %s\n", opcode->assembler);
				return MNEMONIC;
			}
	}

	for (reg=arm_regs;reg->assembler;reg++) {

		if (strlen(token_name) == strlen(reg->assembler))
			if (0 == strncasecmp(reg->assembler,token_name,
					     strlen(token_name)))
				//printf("register: %s\n", reg->assembler);
				return REGISTER;
	}

	return UNKNOWN;
}
