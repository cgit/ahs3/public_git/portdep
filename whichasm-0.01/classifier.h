/* standard types */

enum class {
	UNKNOWN,
	MNEMONIC,
	REGISTER
};

struct token {
	char *name;
	struct token *next;
};

int classifier_arm(char *token_name);
int classifier_ppc(char *token_name);
int classifier_s390x(char *token_name);
int classifier_x86(char *token_name);
