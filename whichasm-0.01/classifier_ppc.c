/*
 * Classifier for ppc assembly language
 *
 * Copyright (C) 2012 Red Hat, Inc.
 *
 */

/* PowerPC/POWER opcodes have a form similar to:
 *    nmemonic <Rt>, <Ra>, <Rb>...
 * Register names include the following (32-bit is notional):
 *    32/64-bit GPRS: GPR0-GPR31
 *    32/64-bit SPRS: SPRG4-SPRG7
 *    32-bit Condition register: CR
 *    32/64-bit Link Register: LR
 *    32/64-bit Count Register: CTR
 *    32/64-bit Fixed-Point Exception Register: XER
 *    32-bit VR Save Register: VRSAVE
 *    32/64-bit Floating Point: FPR0-FPR31
 *    32/64-bit Vector registers: VR0-VR31
 *    32-bit Vector Status control: VSCR
 *    32/64-bit Vector scalar registers: VSR0-VSR63
 */

#include "ppc.h"
#include <stdio.h>
#include <sys/types.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>

#include "classifier.h"

int classifier_ppc(char *token_name)
{
        const struct ppc_opcode *opcode;
        const struct ppc_reg *reg;

        for (opcode=ppc_opcodes;opcode->assembler;opcode++) {
		// TODO - catch variants of assembly language mnemonics
		// previously limited to opcode->assembler length
		// need to catch assembly that has appended letters
		if (strlen(token_name) == strlen(opcode->assembler))
			if (0 == strncasecmp(token_name, opcode->assembler,
					     strlen(opcode->assembler))) {
				//printf("opcode: %s\n", opcode->assembler);
                        	return MNEMONIC;
			}
        }

        for (reg=ppc_regs;reg->assembler;reg++) {
		if (strlen(token_name) == strlen(reg->assembler))
			if (0 == strncasecmp(reg->assembler,token_name,
					     strlen(token_name))) {
				//printf("ppc register match\n");
                        	return REGISTER;
			}
        }

        return UNKNOWN;
}

