/*
 * Classifier for s390x assembly language
 *
 * Copyright (C) 2012 Red Hat, Inc.
 *
 */

/* s390x opcodes have 26 different formats but mostly match this:
 *    nmemonic <param1>, <param2>, <param3>...
 * Register names include the following:
 *    32/64-bit GPRS: R0-R15 (GPR0-GPR15)
 *    32/64-bit control registers: CR0-CR15
 *    32-bit access registers: AR0-AR15
 *    64-bit floating point: FP0-FP15
 *    64/128-bit status reg: PSW
 */

#include "s390x.h"
#include <stdio.h>
#include <sys/types.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>

#include "classifier.h"

int classifier_s390x(char *token_name)
{
        const struct s390x_opcode *opcode;
        const struct s390x_reg *reg;

        for (opcode=s390x_opcodes;opcode->assembler;opcode++) {
		// TODO - catch variants of assembly language mnemonics
		// previously limited to opcode->assembler length
		// need to catch assembly that has appended letters
		if (strlen(token_name) == strlen(opcode->assembler))
			if (0 == strncasecmp(token_name, opcode->assembler,
					     strlen(opcode->assembler))) {
				//printf("opcode: %s\n", opcode->assembler);
                        	return MNEMONIC;
			}
        }

        for (reg=s390x_regs;reg->assembler;reg++) {
		if (strlen(token_name) == strlen(reg->assembler))
			if (0 == strncasecmp(reg->assembler,token_name,
					     strlen(token_name)))
                        return REGISTER;
        }

        return UNKNOWN;
}

