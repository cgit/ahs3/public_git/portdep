#!/bin/bash
#
#   Copyright (c) 2012, Al Stone <ahs3@ahs3.net>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of version 2 (only) of the GNU General
#   Public License as published by the Free Software Foundation.
#
#
#       Sort through the raw results of a scan and list all of
#       packages found and their reported versions
#

if [ "$#" -lt 1 ]
then
        echo "usage: ./report-versions.sh <results-file>"
        exit 1
fi

data="$1"
if [ ! -s "$data" ]
then
        echo "? has to be something to work with"
        exit 1
fi

cat "$data" | (
        declare -i count
        count=0
        while IFS='' read -r line || [ -n "$line" ]
        do
                case "$line" in
                Package:*)
                        [ $count -gt 0 ] && echo $pkg $version
                        count+=1
                        pkg=$(echo $line | sed 's/Package: //')
                        ;;
                Source:*)
                        version=$(echo $line | sed 's/Source: //' | \
                                  sed "s/${pkg}-//")
                        ;;
                esac
        done
)

